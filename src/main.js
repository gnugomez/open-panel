import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import VueNotificationList from "@dafcoe/vue-notification";
import "@dafcoe/vue-notification/dist/vue-notification.css";

/* Firebase */
import firebase from "firebase";

firebase.initializeApp({
  apiKey: "AIzaSyDo61HzvrJG2qlSbQAGZwZZCt9bhF0tEak",
  authDomain: "open-panel.firebaseapp.com",
  projectId: "open-panel",
  storageBucket: "open-panel.appspot.com",
  messagingSenderId: "1030699527819",
  appId: "1:1030699527819:web:aa9612b89d788d34646e8e",
  measurementId: "G-VMZHE48JLD",
});

firebase.getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      unsubscribe();
      resolve(user);
    }, reject);
  });
};

import "./assets/scss/theme.scss";

createApp(App).use(store).use(router).use(VueNotificationList).mount("#app");
