module.exports = {
  publicPath: "",
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        productName: "Open Panel",
        appId: "com.gnugomez.openpanel",
      },
    },
  },
};
